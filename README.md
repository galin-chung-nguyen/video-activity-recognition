# Video Activity Recognition

Our contribution in this video activity recognition model is two-fold.
- First, we propose an improved two-stream ConvNet architecture: one stream is a normal image classification ConvNet being fed still frames to classify static appearance, the ConvNet is ensembled from four base CNNs model into a strong classifier by summing their probility prediction, hence named EnsembleNet; another one is called motion stream capable of capturing and recognizing motion based on embedded batches of frames.
- Second, we built a brand new dataset (SEAGS-V1) of Southeast Asian Sports short videos, consisting of both standard videos with no effect and non-standard videos with effects - a modern factor that all currently available datasets being used for benchmarking models lack of. Our model is trained and evaluated in combination with different backbone architectures and on two benchmarks: UCF- 101 and SEAGS-V1. The result derived shows that this is a model with competitive perfomance compared to previous attempts to use deep nets for human activity recognition in short-form videos.

## Model architecture
<img src="./assets/images/readme/overview.png" style="max-width:700px">

## SEAGS-V1 dataset statistics
<img src="./assets/images/readme/seags_clipamount.png" style="max-width:700px">
<img src="./assets/images/readme/chart.png" style="max-width:700px">
<img src="./assets/images/readme/seags_avgclipdura.png" style="max-width:700px">
<img src="./assets/images/readme/seags_totaltime.png" style="max-width:700px">

## Results
<img src="./assets/images/readme/result-2.png" style="max-width:400px">
<img src="./assets/images/readme/result-3.png" style="max-width:400px">
<img src="./assets/images/readme/result-4.png" style="max-width:400px">
<img src="./assets/images/readme/result-5.png" style="max-width:400px">

## Contributors
Thanks to the following people who have contributed to this project:

* [@loc4atnt](https://github.com/loc4atnt) 📖
* [@trislee02](https://github.com/trislee02) 🐛

## Contact

If you want to contact me you can reach out to me at [Linkedin](https://www.linkedin.com/in/galin-chung-nguyen/).

### **Have a good day!**

<!-- ## License

This project uses the following license: [<license_name>](<link>). -->
<!-- # Donus

Donus is a free web-based messaging app for everyone. It's built using React, Redux and Firebase storage. You can use it to make chat group with your friends, send and receive messages instantly, do cool things with it. Feel free to discover and enjoy it!

#### Login screen

![Login screen](./assets/readme/demoScreen_logIn.png)

#### Chat

![Chat](./assets/readme/chat.png)

#### Invite new friends to the chat

![Invite new friends to the chat](./assets/readme/invite-friend.png)

#### Join a chat

![Join a chat](./assets/readme/inviteScreen.png)

#### Chat member settings

![Chat member settings](./assets/readme/change-role.png)

You can check it out here: https://donus-chat.web.app/

### **Have a good day!**
 -->
